@extends('layouts.front-end.app')

@section('title','Terms & Conditions')

@push('css_or_js')
    <meta property="og:image" content="{{asset('storage/app/public/company')}}/{{$web_config['web_logo']->value}}"/>
    <meta property="og:title" content="Terms & conditions of {{$web_config['name']->value}} "/>
    <meta property="og:url" content="{{env('APP_URL')}}">
    <meta property="og:description" content="{!! substr($web_config['about']->value,0,100) !!}">

    <meta property="twitter:card" content="{{asset('storage/app/public/company')}}/{{$web_config['web_logo']->value}}"/>
    <meta property="twitter:title" content="Terms & conditions of {{$web_config['name']->value}}"/>
    <meta property="twitter:url" content="{{env('APP_URL')}}">
    <meta property="twitter:description" content="{!! substr($web_config['about']->value,0,100) !!}">

    <style>
        .headerTitle {
            font-size: 25px;
            font-weight: 700;
            margin-top: 2rem;
        }

        .for-container {
            width: 91%;
            border: 1px solid #D8D8D8;
            margin-top: 3%;
            margin-bottom: 3%;
        }

        .for-padding {
            padding: 3%;
        }
    </style>
@endpush

@section('content')
    <div class="container for-container">
        <h2 class="text-center mt-3 headerTitle">Returns and Cancellation Policy</h2>
        <div class="for-padding">
            <p>Our policy lasts 7 days. If 7 days have gone by since your purchase, unfortunately we can’t offer you a refund or exchange.</p>

            <p>To be eligible for a return, your item must be unused and in the same condition that you received it. It must also be in the original packaging.</p>

            <p> Several types of goods are exempt from being returned. Perishable goods such as food, flowers, newspapers or magazines cannot be returned. We also do not accept products that are intimate or sanitary goods, hazardous materials, or flammable liquids or gases.</p>
            <p>To complete your return, we require a receipt or proof of purchase.</p>
            <p>Any item not in its original condition is damaged or missing parts for reasons not due to our error.</p>
                <p>Any item that is returned more than 7 days after delivery</p>

            <p><strong>Refunds (if applicable)</strong></p>

            <p>Once your return is received and inspected, we will send you an email to notify you that we have received your returned item. We will also notify you of the approval or rejection of your refund.</p>

            <p>If you are approved, then your refund will be processed, and a credit will automatically be applied to your credit card or original method of payment, within a certain amount of days.</p>



            <p><strong>Late or missing refunds (if applicable)</strong></p>

            <p>If you haven’t received a refund yet, first check your bank account again.</p>

            <p>Then contact your credit card company, it may take some time before your refund is officially posted.</p>

            <p>Next contact your bank. There is often some processing time before a refund is posted.</p>

            <p>If you’ve done all of this and you still have not received your refund yet, please contact us at [support@nativekart.in]. </p>

            <p><strong>Sale items (if applicable)</strong></p>

            <p>Only regular priced items may be refunded, unfortunately sale items cannot be refunded.</p>

            <p><strong>Exchanges (if applicable)</strong></p>

            <p>We only replace items if they are defective or damaged.  If you need to exchange it for the same item, send us an email at [support@nativekart.in] and send your item to: 761, Sri Sai Ram Mansion, First Floor, above Andhra Bank, Marathahalli, Bengaluru, Karnataka 560037</p>


            <p><strong>Shipping</strong></p>

            <p>To return your product, you should mail your product to: 761, Sri Sai Ram Mansion, First Floor, above Andhra Bank, Marathahalli, Bengaluru, and Karnataka 560037</p>

            <p>You will be responsible for paying for your own shipping costs for returning your item. Shipping costs are non-refundable. If you receive a refund, the cost of return shipping will be deducted from your refund.</p>

            <p>Depending on where you live, the time it may take for your exchanged product to reach you, may vary.</p>
        </div>
    </div>
@endsection
