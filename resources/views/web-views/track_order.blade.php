@extends('layouts.front-end.app')

@section('title','Track order')

@push('css_or_js')
    <meta property="og:image" content="{{asset('storage/app/public/company')}}/{{$web_config['web_logo']->value}}"/>
    <meta property="og:title" content="Track order of {{$web_config['name']->value}} "/>
    <meta property="og:url" content="{{env('APP_URL')}}">
    <meta property="og:description" content="{!! substr($web_config['about']->value,0,100) !!}">

    <meta property="twitter:card" content="{{asset('storage/app/public/company')}}/{{$web_config['web_logo']->value}}"/>
    <meta property="twitter:title" content="Track order of {{$web_config['name']->value}}"/>
    <meta property="twitter:url" content="{{env('APP_URL')}}">
    <meta property="twitter:description" content="{!! substr($web_config['about']->value,0,100) !!}">

    <style>
        .headerTitle {
            font-size: 25px;
            font-weight: 700;
            margin-top: 2rem;
        }

        .for-container {
            width: 91%;
            border: 1px solid #D8D8D8;
            margin-top: 3%;
            margin-bottom: 3%;
        }

        .for-padding {
            padding: 3%;
        }
    </style>
@endpush

@section('content')
    <div class="container for-container">
        <h2 class="text-center mt-3 headerTitle">Our Courier Partner</h2>
         <div class="row" >
            <div class="col-4">
                <div style="background: white; border-radius: 5px">
                    <a href="https://www.delhivery.com/?utm_source=dlv_website&utm_medium=homepage_businesses_tab" target="_blank"><img src="{{asset("storage/app/public/png/nativekart_delhivery.png")}}" alt=""></a>
                </div>
            </div>
            <div class="col-4">
                <div style="background: white; border-radius: 5px">
                    <a href="https://ecomexpress.in/" target="_blank"><img src="{{asset("storage/app/public/png/nativekart_ecom_express.png")}}" alt=""></a>
                </div>
            </div>
            <div class="col-4">
                <div style="background: white; border-radius: 5px">
                    <a href="https://www.xpressbees.com/track" target="_blank"><img src="{{asset("storage/app/public/png/nativekart_xpressbees.png")}}" alt=""></a>
                </div>
            </div>
        </div>
    </div>
@endsection
