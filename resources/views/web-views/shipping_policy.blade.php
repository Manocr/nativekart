@extends('layouts.front-end.app')

@section('title','Terms & Conditions')

@push('css_or_js')
    <meta property="og:image" content="{{asset('storage/app/public/company')}}/{{$web_config['web_logo']->value}}"/>
    <meta property="og:title" content="Terms & conditions of {{$web_config['name']->value}} "/>
    <meta property="og:url" content="{{env('APP_URL')}}">
    <meta property="og:description" content="{!! substr($web_config['about']->value,0,100) !!}">

    <meta property="twitter:card" content="{{asset('storage/app/public/company')}}/{{$web_config['web_logo']->value}}"/>
    <meta property="twitter:title" content="Terms & conditions of {{$web_config['name']->value}}"/>
    <meta property="twitter:url" content="{{env('APP_URL')}}">
    <meta property="twitter:description" content="{!! substr($web_config['about']->value,0,100) !!}">

    <style>
        .headerTitle {
            font-size: 25px;
            font-weight: 700;
            margin-top: 2rem;
        }

        .for-container {
            width: 91%;
            border: 1px solid #D8D8D8;
            margin-top: 3%;
            margin-bottom: 3%;
        }

        .for-padding {
            padding: 3%;
        }
    </style>
@endpush

@section('content')
    <div class="container for-container">
        <h2 class="text-center mt-3 headerTitle">Shipping Policy</h2>
        <div class="for-padding">
            <p><strong>What are the delivery charges?</strong></p>

            <p>We will change the Rs. 40 charges for delivery per item is applied. </p>

            <p>Why does the delivery date not correspond to the delivery timeline of X-Y business days?</p>

            <p>It is possible that the our courier partners have a holiday between the day your placed your order and the date of delivery, which is based on the timelines shown on the product page. In this case, we add a day to the estimated date. Some courier partners do not work on Sundays and this is factored in to the delivery dates.</p>


            <p><strong>What is the estimated delivery time?</strong></p>

            <p>We will generally procure and ship the items within the time specified on the product page. Business days exclude public holidays and Sundays</p>

            <p>We will deliver the order to the customer (Min 3Day and Max 5Days)</p>

            <p><strong>Why is the CoD option not offered?</strong></p>

            <p>Currently we are not offering the COD option; we will update you once we will provide you the COD payment facilities.</p>


            <p><strong>Payment Policy</strong></p>

            <p>NativeKart.in offers you multiple payment methods. Whatever your online mode of payment, you can rest assured that NativeKart trusted payment gateway partners use secure encryption technology to keep your transaction details confidential at all times.</p>

            <p>You may use Internet Banking to make your purchase.</p>

            <p><strong>Are there any hidden charges?</strong></p>

            <p>There are NO hidden charges when you make a purchase on NativeKart. The prices listed for all the items are final and all-inclusive. The price you see on the product page is exactly.</p>


        </div>
    </div>
@endsection
