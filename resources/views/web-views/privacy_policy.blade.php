@extends('layouts.front-end.app')

@section('title','Terms & Conditions')

@push('css_or_js')
    <meta property="og:image" content="{{asset('storage/app/public/company')}}/{{$web_config['web_logo']->value}}"/>
    <meta property="og:title" content="Terms & conditions of {{$web_config['name']->value}} "/>
    <meta property="og:url" content="{{env('APP_URL')}}">
    <meta property="og:description" content="{!! substr($web_config['about']->value,0,100) !!}">

    <meta property="twitter:card" content="{{asset('storage/app/public/company')}}/{{$web_config['web_logo']->value}}"/>
    <meta property="twitter:title" content="Terms & conditions of {{$web_config['name']->value}}"/>
    <meta property="twitter:url" content="{{env('APP_URL')}}">
    <meta property="twitter:description" content="{!! substr($web_config['about']->value,0,100) !!}">

    <style>
        .headerTitle {
            font-size: 25px;
            font-weight: 700;
            margin-top: 2rem;
        }

        .for-container {
            width: 91%;
            border: 1px solid #D8D8D8;
            margin-top: 3%;
            margin-bottom: 3%;
        }

        .for-padding {
            padding: 3%;
        }
    </style>
@endpush

@section('content')
    <div class="container for-container">
        <h2 class="text-center mt-3 headerTitle">Privacy Policy</h2>
        <div class="for-padding">
            <p>This privacy policy only covers information collected in Native Kart LLP, and does not cover any information collected offline by Native Kart LLP. We respect your privacy; we have implemented procedures to ensure that your personal information is handled in a safe, secure, and responsible manner. This Privacy Policy explains about our data processing practices and the ways in which your personal data is being used.</p>

            <p><strong>Information that We Collect from You</strong></p>

            <p>There are some of our services which you may be able to use without providing any personal information to us. However, in order to take advantage of certain features, offerings, or other functions of our Services, you may be asked or required to provide some of the personal information.</p>

            <p> Name</p>
            <p>Mobile no</p>
            <p>Contact information including email address</p>
                <p>Demographic information such as postcode, preferences and interests</p>
                    <p>Other information relevant to customer surveys and/or offers</p>

                    <p><strong>Information that We Collect from You</strong></p>

            <p>If you choose a direct payment gateway to complete your purchase, then Grocer4u/Razorpay stores your credit card data. It is encrypted through the Payment Card Industry Data Security Standard (PCI-DSS). Your purchase transaction data is stored only as long as is necessary to complete your purchase transaction. After that is complete, your purchase transaction information is deleted.</p>

            <p>All direct payment gateways adhere to the standards set by PCI-DSS as managed by the PCI Security Standards Council, which is a joint effort of brands like Visa, MasterCard, Repay card.</p>

            <p>PCI-DSS requirements help ensure the secure handling of credit card information by our store and its service providers.</p>

            <p>For more insight, you may also want to read Razorpay’s Terms of Service </p>

            <p><strong>How we use the information we collect</strong></p>

            <p>The information is used for internal purposes only. We will never sell or rent this information to any other organization. Your data are stored in databases of Native Kart LLP application.</p>

            <p>We may use the information to improve our products and services.</p>

            <p>With your permission we may periodically send promotional emails about new products, special offers or other information which we think you may find interesting using the email address which you have provided.</p>

            <p>From time to time, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail. We may use the information to customize the website according to your interest </p>

            <p><strong>Security</strong></p>

            <p>To protect your personal information, we take reasonable precautions and follow industry best practices to make sure it is not inappropriately lost, misused, accessed, disclosed, altered or destroyed.</p>

            <p><strong>Changes to the privacy policy</strong></p>

            <p>We reserve the right to modify this privacy policy at any time, so please review it frequently. Changes and clarifications will take effect immediately upon their posting on the website. If we make material changes to this policy, we will notify you here that it has been updated, so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we use and/or disclose it.</p>

            <p>If our store is acquired or merged with another company, your information may be transferred to the new owners so that we may continue to sell products to you.</p>

            <p><strong>Reach Us</strong></p>

            <p>If you have any questions or concerns regarding this privacy policy, you should contact us by sending an e-mail to support@nativekart.in</p>
        </div>
    </div>
@endsection
