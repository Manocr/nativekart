<?php

namespace App\Http\Controllers\Admin;

use App\CPU\ImageManager;
use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\Pincode;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    public function index()
    {
        ImageManager::cleanSession();
        $categories=Category::where(['position'=>0])->paginate(10);
        return view('admin-views.category.view',compact('categories'));
    }

    public function store(Request $request)
    {
        $category = new Category;
        $category->name = $request->name;
        $category->slug = Str::slug($request->name);
        $x = ImageManager::upload('category/', 'png', 'category_icon_modal');
        $category->icon = $x[0];
        $category->parent_id = 0;
        $category->position = 0;
        $category->save();
        return response()->json();
    }

    public function edit(Request $request)
    {
        $data = Category::where('id', $request->id)->first();
        return response()->json($data);
    }

    public function update(Request $request)
    {
        $category = Category::find($request->id);
        $category->name = $request->name;
        $category->slug = Str::slug($request->name);
        $x = ImageManager::update('category/', $category->icon, 'png','category_icon_modal');
        $category->icon = $x[0];
        $category->parent_id = 0;
        $category->position = 0;
        $category->save();
        return response()->json();
    }

    public function delete(Request $request)
    {
        $categories = Category::where('parent_id', $request->id)->get();
        if (!empty($categories)) {

            foreach ($categories as $category) {
                $categories1 = Category::where('parent_id', $category->id)->get();
                if (!empty($categories1)) {
                    foreach ($categories1 as $category1) {
                        Category::destroy($category1->id);
                    }
                }
                Category::destroy($category->id);
            }
        }
        Category::destroy($request->id);
        return response()->json();
    }

    public function fetch(Request $request)
    {
        if ($request->ajax()) {
            $data = Category::where('position', 0)->orderBy('id', 'desc')->get();
            return response()->json($data);
        }
    }



    // Pincode

    public function pincode_list()
    {
        $pincode=Pincode::get();
        return view('admin-views.pincode.list',compact('pincode'));
    }

    public function check_pincode_list(Request $request)
    {

        $pincode=Pincode::where('pincode', $request->pincode)->first();

        if($pincode){
            return response()->json(array(
                "error"=>false,
                "message"=>"Delivery is available"
            ));
        }else{
            return response()->json(array(
                "error"=>true,
                "message"=>"Delivery is not available"
            ));
        }

        // return view('admin-views.pincode.list',compact('pincode'));
    }


    public function pincode_store(Request $request)
    {
        $category = new Pincode;
        $category->pincode = $request->name;
        $category->save();
        return response()->json();
    }

    public function pincode_edit(Request $request)
    {
        $data = Pincode::where('id', $request->id)->first();
        return response()->json($data);
    }

    public function pincode_update(Request $request)
    {
        $category = Pincode::find($request->id);
        $category->pincode = $request->name;
        $category->save();
        return response()->json();
    }

    public function pincode_delete(Request $request)
    {
        $categories = Pincode::where('id', $request->id)->get();
        if (!empty($categories)) {

            foreach ($categories as $category) {
                $categories1 = Pincode::where('id', $category->id)->get();
                if (!empty($categories1)) {
                    foreach ($categories1 as $category1) {
                        Pincode::destroy($category1->id);
                    }
                }
                Pincode::destroy($category->id);
            }
        }
        Pincode::destroy($request->id);
        return response()->json();
    }

    public function pincode_fetch(Request $request)
    {
        if ($request->ajax()) {
            $data = Pincode::orderBy('id', 'desc')->get();
            return response()->json($data);
        }
    }
}
